//Copyright (c) 2020 Stefan Thesing
//
//This file is part of libzettels.
//
//libzettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//libzettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with libzettels. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------

extern crate libzettels;
extern crate tempfile;
use self::tempfile::tempdir;

use libzettels::{Config, Index, IndexingMethod};
use libzettels::examples::*;
use std::path::PathBuf;
    
#[test]
fn test_gitignore_grep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten");
    
    let mut cfg = Config::new(rootdir.to_str().unwrap(), "foo");
    cfg.indexingmethod = IndexingMethod::Grep;
    let index = Index::new(&cfg)
        .expect("Failed to create index.");        
    assert_eq!(index.files.len(), 7);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));   
}

#[test]
fn test_gitignore_ripgrep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten");
    
    let mut cfg = Config::new(rootdir.to_str().unwrap(), "foo");
    cfg.indexingmethod = IndexingMethod::RipGrep;
    let index = Index::new(&cfg)
        .expect("Failed to create index.");        
    assert_eq!(index.files.len(), 7);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));   
}

#[test]
fn test_gitignore_native() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten");
    
    let mut cfg = Config::new(rootdir.to_str().unwrap(), "foo");
    cfg.indexingmethod = IndexingMethod::Native;
    let index = Index::new(&cfg)
        .expect("Failed to create index.");        
    assert_eq!(index.files.len(), 7);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));   
}

#[test]
fn test_zettelsignore_grep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten");
    
    let mut cfg = Config::new(rootdir.to_str().unwrap(), "foo");
    cfg.indexingmethod = IndexingMethod::Grep;
    cfg.ignorefile = PathBuf::from(".zettelsignore");
    let index = Index::new(&cfg)
        .expect("Failed to create index.");  
    assert_eq!(index.files.len(), 7);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));
}

#[test]
fn test_zettelsignore_ripgrep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten");
    
    let mut cfg = Config::new(rootdir.to_str().unwrap(), "foo");
    cfg.indexingmethod = IndexingMethod::RipGrep;
    cfg.ignorefile = PathBuf::from(".zettelsignore");
    let index = Index::new(&cfg)
        .expect("Failed to create index.");  
    assert_eq!(index.files.len(), 7);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));
}

#[test]
fn test_zettelsignore_native() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten");
    
    let mut cfg = Config::new(rootdir.to_str().unwrap(), "foo");
    cfg.indexingmethod = IndexingMethod::Native;
    cfg.ignorefile = PathBuf::from(".zettelsignore");
    let index = Index::new(&cfg)
        .expect("Failed to create index.");  
    assert_eq!(index.files.len(), 7);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));
}

#[test]
fn test_nonexsistent_ignorefile_grep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten");
    
    let mut cfg = Config::new(rootdir.to_str().unwrap(), "foo");
    cfg.indexingmethod = IndexingMethod::Grep;
    cfg.ignorefile = PathBuf::from(".bar");  // doesn't exist
    let index = Index::new(&cfg)
        .expect("Failed to create index."); 
    assert_eq!(index.files.len(), 12);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));
    assert!(index.files.contains_key(&PathBuf::from(".gitignore")));
    assert!(index.files.contains_key(&PathBuf::from(".hiddendir/.hiddenfile2.txt")));
    assert!(index.files.contains_key(&PathBuf::from(".hiddendir/a-file.txt")));
    assert!(index.files.contains_key(&PathBuf::from(".hiddenfile.txt")));
    assert!(index.files.contains_key(&PathBuf::from(".zettelsignore")));
}

#[test]
fn test_nonexsistent_ignorefile_ripgrep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten");
    
    let mut cfg = Config::new(rootdir.to_str().unwrap(), "foo");
    cfg.indexingmethod = IndexingMethod::RipGrep;
    cfg.ignorefile = PathBuf::from(".bar");  // doesn't exist
    let index = Index::new(&cfg)
        .expect("Failed to create index."); 
    assert_eq!(index.files.len(), 12);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));
    assert!(index.files.contains_key(&PathBuf::from(".gitignore")));
    assert!(index.files.contains_key(&PathBuf::from(".hiddendir/.hiddenfile2.txt")));
    assert!(index.files.contains_key(&PathBuf::from(".hiddendir/a-file.txt")));
    assert!(index.files.contains_key(&PathBuf::from(".hiddenfile.txt")));
    assert!(index.files.contains_key(&PathBuf::from(".zettelsignore")));
}

#[test]
fn test_nonexistent_ignorefile_native() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten");
    
    let mut cfg = Config::new(rootdir.to_str().unwrap(), "foo");
    cfg.indexingmethod = IndexingMethod::Native;
    cfg.ignorefile = PathBuf::from(".bar");  // doesn't exist
    let index = Index::new(&cfg)
        .expect("Failed to create index."); 
    assert_eq!(index.files.len(), 12);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));
    assert!(index.files.contains_key(&PathBuf::from(".gitignore")));
    assert!(index.files.contains_key(&PathBuf::from(".hiddendir/.hiddenfile2.txt")));
    assert!(index.files.contains_key(&PathBuf::from(".hiddendir/a-file.txt")));
    assert!(index.files.contains_key(&PathBuf::from(".hiddenfile.txt")));
    assert!(index.files.contains_key(&PathBuf::from(".zettelsignore")));
}