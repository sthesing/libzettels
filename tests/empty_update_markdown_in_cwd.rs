//Copyright (c) 2020 Stefan Thesing
//
//This file is part of libzettels.
//
//libzettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//libzettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with libzettels. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------

extern crate libzettels;
extern crate tempfile;
use self::tempfile::tempdir;

use libzettels::{Config, Index, IndexingMethod};
use libzettels::examples::*;
use std::io::Write;

const FILE7: &str ="# A totally unrelated file, which just happens to be in
the working directory and also happens to contain a markdown link to a file
that is not [contained in the zettelkasten](unrelated_file.md).";

#[test]
fn test_empty_update_markdown_in_cwd_ripgrep() {
    empty_update_markdown_in_cwd(IndexingMethod::RipGrep);
}

#[test]
fn test_empty_update_markdown_in_cwd_grep() {
    empty_update_markdown_in_cwd(IndexingMethod::Grep);
}

#[test]
fn test_empty_update_markdown_in_cwd_native() {
    empty_update_markdown_in_cwd(IndexingMethod::Native);
}

fn empty_update_markdown_in_cwd(indexingmethod: IndexingMethod) {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_examples_with_index(dir).expect("Failed to generate examples");
    
    let config_dir = dir.join("examples/config");
    let cfg_file = config_dir.join("libzettels.cfg.yaml");
    let mut cfg = Config::from_file(cfg_file).expect("Failed to load config");
    cfg.indexingmethod = indexingmethod;
    
    let mut index = Index::load(&cfg).expect("Failed to load index");
    
    let working_dir = dir.join("foo");
    std::fs::create_dir_all(&working_dir)
        .expect("Failed to create working dir");
    
    // A file in the working dir, that contains a markdown link, unrelated
    // to the Zettelkasten
    let mut forbidden_file = std::fs::File::create(&working_dir
                                    .join("forbidden.md"))
                                    .expect("Failed to create forbidden.md");
    writeln!(forbidden_file, "{}", FILE7).expect("Failed to write to file");
    
    // Enter working directory
    std::env::set_current_dir(&working_dir).expect("Failed to enter working dir");
    
    // Call update on a zettelkasten that has not changed.
    let r = index.update(&cfg);
    assert!(r.is_ok());
}