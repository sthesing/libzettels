//Copyright (c) 2020 Stefan Thesing
//
//This file is part of libzettels.
//
//libzettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//libzettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with libzettels. If not, see http://www.gnu.org/licenses/.
// --------------------------------------------------------------------------
extern crate tempfile;
use self::tempfile::tempdir;

use std::fs::{self, File};
use std::io::Write;
use std::thread::sleep;
use std::time::{SystemTime, Duration};

use super::super::*;
use examples::*;

#[test]
fn test_get_list_of_files() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let ignorefile = PathBuf::from(".gitignore");
    let list_of_files = get_list_of_files(&rootdir, &ignorefile);
    assert!(list_of_files.is_ok());
    let list_of_files = list_of_files.unwrap();
    assert_eq!(list_of_files.len(), 7);
    // it should be the following
    assert!(list_of_files.contains(&PathBuf::from("file1.md")));
    assert!(list_of_files.contains(&PathBuf::from("file2.md")));
    assert!(list_of_files.contains(&PathBuf::from("file3.md")));
    assert!(list_of_files.contains(&PathBuf::from("subdir/file4.md")));
    assert!(list_of_files.contains(&PathBuf::from("subdir/file5.md")));
    assert!(list_of_files.contains(&PathBuf::from("onlies/markdown-only.md")));
    assert!(list_of_files.contains(&PathBuf::from("onlies/pure-yaml.yaml")));
}

#[test]
fn test_get_list_of_files_no_ignore() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let _ignorefile = PathBuf::from(".gitignore");
    let list_of_files = get_list_of_files_no_ignore(&rootdir);
    assert!(list_of_files.is_ok());
    let list_of_files = list_of_files.unwrap();
    assert_eq!(list_of_files.len(), 12);
    // it should be the following
    assert!(list_of_files.contains(&PathBuf::from("file1.md")));
    assert!(list_of_files.contains(&PathBuf::from("file2.md")));
    assert!(list_of_files.contains(&PathBuf::from("file3.md")));
    assert!(list_of_files.contains(&PathBuf::from("subdir/file4.md")));
    assert!(list_of_files.contains(&PathBuf::from("subdir/file5.md")));
    assert!(list_of_files.contains(&PathBuf::from("onlies/markdown-only.md")));
    assert!(list_of_files.contains(&PathBuf::from("onlies/pure-yaml.yaml")));
    assert!(list_of_files.contains(&PathBuf::from(".hiddenfile.txt")));
    assert!(list_of_files.contains(&PathBuf::from(".hiddendir/.hiddenfile2.txt")));
    assert!(list_of_files.contains(&PathBuf::from(".hiddendir/a-file.txt")));
    assert!(list_of_files.contains(&PathBuf::from(".gitignore")));
    assert!(list_of_files.contains(&PathBuf::from(".zettelsignore")));
}

#[test]
fn test_get_changed_zettels() {
    let tmp_dir = tempdir().expect("Failed to create temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    
    let ignorefile = PathBuf::from(".gitignore");
    // start with an empty index
    let mut index = Index::empty();
    index.add_zettel(&PathBuf::from("file1.md"),
                        Zettel::new("Test 1"));
    index.add_zettel(&PathBuf::from("subdir/file4.md"),
                        Zettel::new("Test 2"));
    // let's shift the timestamp of the index back in time a bit, because 
    // SystemTime isn't strictly monotonous.
    index.timestamp = SystemTime::now() - Duration::new(5,0); // five seconds.
        
    // Let's go.
    let changed_zettels = get_changed_zettels(&mut index,
                                              &rootdir,
                                              &ignorefile);
    assert!(changed_zettels.is_ok());
    let (zettels_to_update, zettels_to_remove) = changed_zettels.unwrap();
    assert_eq!(zettels_to_update.len(), 7);
    assert_eq!(zettels_to_remove.len(), 0);
    assert!(zettels_to_update.contains(&PathBuf::from("file1.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("file2.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("file3.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("subdir/file4.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("subdir/file5.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("onlies/markdown-only.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("onlies/pure-yaml.yaml")));
        
    // Update the timestamp
    index.timestamp = SystemTime::now();
    let changed_zettels = get_changed_zettels(&mut index,
                                              &rootdir,
                                              &ignorefile);
    assert!(changed_zettels.is_ok());
    let (zettels_to_update, zettels_to_remove) = changed_zettels.unwrap();
    assert_eq!(zettels_to_update.len(), 5);
    assert_eq!(zettels_to_remove.len(), 0);
    assert!(zettels_to_update.contains(&PathBuf::from("file2.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("file3.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("subdir/file5.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("onlies/markdown-only.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("onlies/pure-yaml.yaml")));
        
    // Let's modify test1 and delete test2
    // Because of SystemTime isn't strictly monotonous, let's wait for 
    // 10 milliseconds.
    sleep(Duration::from_millis(10));
    let mut test1 = File::create(rootdir.join("file1.md"))
        .expect("Something went wrong with creating a temporary file for \
        testing.");
    writeln!(test1, "A modification!").expect("Failed to write to file");
    fs::remove_file(rootdir.join("subdir/file4.md"))
        .expect("failed to delete the file.");
    let changed_zettels = get_changed_zettels(&mut index,
                                              &rootdir,
                                              &ignorefile);
    assert!(changed_zettels.is_ok());
    let (zettels_to_update, zettels_to_remove) = changed_zettels.unwrap();
    assert_eq!(zettels_to_update.len(), 6);
    assert!(zettels_to_update.contains(&PathBuf::from("file1.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("file2.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("file3.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("subdir/file5.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("onlies/markdown-only.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("onlies/pure-yaml.yaml")));
    assert_eq!(zettels_to_remove.len(), 1);
    assert!(zettels_to_remove.contains(&PathBuf::from("subdir/file4.md")));
    
    //OK, let's remove test2, to get it out of the way.
    index.remove_zettel(&PathBuf::from("subdir/file4.md"));
    // Update the timestamp
    index.timestamp = SystemTime::now();
    
    // Wait for 10 milliseconds.
    sleep(Duration::from_millis(10));
    //Let's create a new file.
    File::create(rootdir.join("file6.md"))
        .expect("Something went wrong with creating a temporary file for \
        testing.");
    let changed_zettels = get_changed_zettels(&mut index,
                                              &rootdir,
                                              &ignorefile);
    assert!(changed_zettels.is_ok());
    let (zettels_to_update, zettels_to_remove) = changed_zettels.unwrap();
    assert_eq!(zettels_to_remove.len(), 0);
    //println!("Update: {:?}", zettels_to_update);
    assert_eq!(zettels_to_update.len(), 6);
    
    assert!(zettels_to_update.contains(&PathBuf::from("file6.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("file2.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("file3.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("subdir/file5.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("onlies/markdown-only.md")));
    assert!(zettels_to_update.contains(&PathBuf::from("onlies/pure-yaml.yaml")));
}

#[test]
fn test_create_index_grep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    //let tmp_dir = setup_tmp_dir();
    //let rootdir = tmp_dir.path().join("examples/Zettelkasten").to_path_buf();
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let indexingmethod = IndexingMethod::Grep;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_ok());
    let index = index.unwrap();
    assert_eq!(index.files.len(), 7);
    
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    // let's retrieve our two zettels and check the data.
    let z1 = index.get_zettel(PathBuf::from("file1.md"));
    assert!(z1.is_some());
    let z1 = z1.unwrap();
    let z2 = index.get_zettel(PathBuf::from("subdir/file4.md"));
    assert!(z2.is_some());
    let z2= z2.unwrap();
    // Titles
    assert_eq!(z1.title, "File 1");
    assert_eq!(z2.title, "File 4");
    // Keywords
    assert_eq!(z1.keywords.len(), 3);
    assert!(z1.keywords.contains(&String::from("example")));
    assert!(z1.keywords.contains(&String::from("first")));
    assert!(z1.keywords.contains(&String::from("test")));
    assert_eq!(z2.keywords.len(), 2);
    assert!(z2.keywords.contains(&String::from("example")));
    assert!(z2.keywords.contains(&String::from("fourth")));
    // Followups
    assert_eq!(z1.followups.len(), 2);
    assert!(z1.followups.contains(&PathBuf::from("file2.md")));
    assert!(z1.followups.contains(&PathBuf::from("file3.md")));
    assert_eq!(z2.followups.len(), 2);
    assert!(z2.followups.contains(&PathBuf::from("file1.md")));
    assert!(z2.followups.contains(&PathBuf::from("subdir/file5.md")));
    // So far for the things defined in the YAML-Metadata.
    // Let's check for targets of markdown hyperlinks
    assert_eq!(z1.links.len(), 2);
    assert!(z1.links.contains(&PathBuf::from("file2.md")));    
    assert!(z1.links.contains(&PathBuf::from("file3.md")));    
    assert_eq!(z2.links.len(), 1);
    assert!(z2.links.contains(&PathBuf::from("file1.md")));
}

#[test]
fn test_create_index_ripgrep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    //let tmp_dir = setup_tmp_dir();
    //let rootdir = tmp_dir.path().join("examples/Zettelkasten").to_path_buf();
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let indexingmethod = IndexingMethod::RipGrep;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_ok());
    let index = index.unwrap();
    assert_eq!(index.files.len(), 7);
    
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    // let's retrieve our two zettels and check the data.
    let z1 = index.get_zettel(PathBuf::from("file1.md"));
    assert!(z1.is_some());
    let z1 = z1.unwrap();
    let z2 = index.get_zettel(PathBuf::from("subdir/file4.md"));
    assert!(z2.is_some());
    let z2= z2.unwrap();
    // Titles
    assert_eq!(z1.title, "File 1");
    assert_eq!(z2.title, "File 4");
    // Keywords
    assert_eq!(z1.keywords.len(), 3);
    assert!(z1.keywords.contains(&String::from("example")));
    assert!(z1.keywords.contains(&String::from("first")));
    assert!(z1.keywords.contains(&String::from("test")));
    assert_eq!(z2.keywords.len(), 2);
    assert!(z2.keywords.contains(&String::from("example")));
    assert!(z2.keywords.contains(&String::from("fourth")));
    // Followups
    assert_eq!(z1.followups.len(), 2);
    assert!(z1.followups.contains(&PathBuf::from("file2.md")));
    assert!(z1.followups.contains(&PathBuf::from("file3.md")));
    assert_eq!(z2.followups.len(), 2);
    assert!(z2.followups.contains(&PathBuf::from("file1.md")));
    assert!(z2.followups.contains(&PathBuf::from("subdir/file5.md")));
    // So far for the things defined in the YAML-Metadata.
    // Let's check for targets of markdown hyperlinks
    assert_eq!(z1.links.len(), 2);
    assert!(z1.links.contains(&PathBuf::from("file2.md")));    
    assert!(z1.links.contains(&PathBuf::from("file3.md")));    
    assert_eq!(z2.links.len(), 1);
    assert!(z2.links.contains(&PathBuf::from("file1.md")));
}

#[test]
fn test_create_index_native() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    //let tmp_dir = setup_tmp_dir();
    //let rootdir = tmp_dir.path().join("examples/Zettelkasten").to_path_buf();
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let indexingmethod = IndexingMethod::Native;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_ok());
    let index = index.unwrap();
    assert_eq!(index.files.len(), 7);
    
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    // let's retrieve our two zettels and check the data.
    let z1 = index.get_zettel(PathBuf::from("file1.md"));
    assert!(z1.is_some());
    let z1 = z1.unwrap();
    let z2 = index.get_zettel(PathBuf::from("subdir/file4.md"));
    assert!(z2.is_some());
    let z2= z2.unwrap();
    // Titles
    assert_eq!(z1.title, "File 1");
    assert_eq!(z2.title, "File 4");
    // Keywords
    assert_eq!(z1.keywords.len(), 3);
    assert!(z1.keywords.contains(&String::from("example")));
    assert!(z1.keywords.contains(&String::from("first")));
    assert!(z1.keywords.contains(&String::from("test")));
    assert_eq!(z2.keywords.len(), 2);
    assert!(z2.keywords.contains(&String::from("example")));
    assert!(z2.keywords.contains(&String::from("fourth")));
    // Followups
    assert_eq!(z1.followups.len(), 2);
    assert!(z1.followups.contains(&PathBuf::from("file2.md")));
    assert!(z1.followups.contains(&PathBuf::from("file3.md")));
    assert_eq!(z2.followups.len(), 2);
    assert!(z2.followups.contains(&PathBuf::from("file1.md")));
    assert!(z2.followups.contains(&PathBuf::from("subdir/file5.md")));
    // So far for the things defined in the YAML-Metadata.
    // Let's check for targets of markdown hyperlinks
    assert_eq!(z1.links.len(), 2);
    assert!(z1.links.contains(&PathBuf::from("file2.md")));    
    assert!(z1.links.contains(&PathBuf::from("file3.md")));    
    assert_eq!(z2.links.len(), 1);
    assert!(z2.links.contains(&PathBuf::from("file1.md")));
}

#[test]
fn test_update_index_grep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    //let tmp_dir = setup_tmp_dir();
    //let rootdir = tmp_dir.path().join("examples/Zettelkasten").to_path_buf();
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let indexingmethod = IndexingMethod::Grep;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_ok());
    let mut index = index.unwrap();
    // Because of SystemTime isn't strictly monotonous, let's wait for ten ms
    sleep(Duration::from_millis(10));
    // rename one
    fs::rename(rootdir.join("file2.md"),
                rootdir.join("ren_file2.md"))
        .expect("Error deleting a temporary file.");
    // modifiy one
    fs::write(rootdir.join("file1.md"), "---
title:  'Modified File 1'
author: Stefan Thesing
keywords: [example1]
followups: [ren_file2.md]
lang: de
...

# Bla bla

Lorem ipsum.")
        .expect("Something went wrong overwriting the contents of Test 1");
    // create a new one
    let mut f6 = File::create(rootdir.join("file6.md"))
        .expect("Something went wrong with creating a temporary file for \
        testing.");
    writeln!(f6, "---
title:  'File 6'
author: Stefan Thesing
keywords: [example3]
followups: []
lang: de
...

# Bla bla

Lorem ipsum.").expect("Failed to write to file");
    // Now, let's update the index
    update_index(&mut index, &indexingmethod, &rootdir, &ignorefile)
        .expect("Received error from function 'update_index'!");
    assert_eq!(index.files.len(), 8);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("ren_file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("file6.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));

    // let's retrieve our two zettels and check the data.
    let z1 = index.get_zettel(PathBuf::from("file1.md"));
    assert!(z1.is_some());
    let z1 = z1.unwrap();
    let z2 = index.get_zettel(PathBuf::from("subdir/file4.md"));
    assert!(z2.is_some());
    let z2= z2.unwrap();
    let z3 = index.get_zettel(PathBuf::from("file6.md"));
    assert!(z3.is_some());
    let z3 = z3.unwrap();
    // Titles
    assert_eq!(z1.title, "Modified File 1");
    assert_eq!(z2.title, "File 4");
    assert_eq!(z3.title, "File 6");
    // Keywords
    assert_eq!(z1.keywords.len(), 1);
    assert!(z1.keywords.contains(&String::from("example1")));
    assert_eq!(z2.keywords.len(), 2);
    assert!(z2.keywords.contains(&String::from("example")));
    assert!(z2.keywords.contains(&String::from("fourth")));
    assert_eq!(z3.keywords.len(), 1);
    assert!(z3.keywords.contains(&String::from("example3")));
    // Followups
    assert_eq!(z1.followups.len(), 1);
    assert!(z1.followups.contains(&PathBuf::from("ren_file2.md")));
    assert_eq!(z2.followups.len(), 2);
    assert!(z2.followups.contains(&PathBuf::from("file1.md")));
    assert!(z2.followups.contains(&PathBuf::from("subdir/file5.md")));
    assert_eq!(z3.followups.len(), 0);
    // So far for the things defined in the YAML-Metadata.
    // Let's check for targets of markdown hyperlinks
    assert_eq!(z1.links.len(), 0);
    assert_eq!(z2.links.len(), 1);
    assert!(z2.links.contains(&PathBuf::from("file1.md")));
    assert_eq!(z3.links.len(), 0);
}
    
#[test]
fn test_update_index_ripgrep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    //let tmp_dir = setup_tmp_dir();
    //let rootdir = tmp_dir.path().join("examples/Zettelkasten").to_path_buf();
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let indexingmethod = IndexingMethod::RipGrep;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_ok());
    let mut index = index.unwrap();
    // Because of SystemTime isn't strictly monotonous, let's wait for ten ms
    sleep(Duration::from_millis(10));
    // rename one
    fs::rename(rootdir.join("file2.md"),
                rootdir.join("ren_file2.md"))
        .expect("Error deleting a temporary file.");
    // modifiy one
    fs::write(rootdir.join("file1.md"), "---
title:  'Modified File 1'
author: Stefan Thesing
keywords: [example1]
followups: [ren_file2.md]
lang: de
...

# Bla bla

Lorem ipsum.")
        .expect("Something went wrong overwriting the contents of Test 1");
    // create a new one
    let mut f6 = File::create(rootdir.join("file6.md"))
        .expect("Something went wrong with creating a temporary file for \
        testing.");
    writeln!(f6, "---
title:  'File 6'
author: Stefan Thesing
keywords: [example3]
followups: []
lang: de
...

# Bla bla

Lorem ipsum.").expect("Failed to write to file");
    // Now, let's update the index
    update_index(&mut index, &indexingmethod, &rootdir, &ignorefile)
        .expect("Received error from function 'update_index'!");
    assert_eq!(index.files.len(), 8);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("ren_file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("file6.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));

    // let's retrieve our two zettels and check the data.
    let z1 = index.get_zettel(PathBuf::from("file1.md"));
    assert!(z1.is_some());
    let z1 = z1.unwrap();
    let z2 = index.get_zettel(PathBuf::from("subdir/file4.md"));
    assert!(z2.is_some());
    let z2= z2.unwrap();
    let z3 = index.get_zettel(PathBuf::from("file6.md"));
    assert!(z3.is_some());
    let z3 = z3.unwrap();
    // Titles
    assert_eq!(z1.title, "Modified File 1");
    assert_eq!(z2.title, "File 4");
    assert_eq!(z3.title, "File 6");
    // Keywords
    assert_eq!(z1.keywords.len(), 1);
    assert!(z1.keywords.contains(&String::from("example1")));
    assert_eq!(z2.keywords.len(), 2);
    assert!(z2.keywords.contains(&String::from("example")));
    assert!(z2.keywords.contains(&String::from("fourth")));
    assert_eq!(z3.keywords.len(), 1);
    assert!(z3.keywords.contains(&String::from("example3")));
    // Followups
    assert_eq!(z1.followups.len(), 1);
    assert!(z1.followups.contains(&PathBuf::from("ren_file2.md")));
    assert_eq!(z2.followups.len(), 2);
    assert!(z2.followups.contains(&PathBuf::from("file1.md")));
    assert!(z2.followups.contains(&PathBuf::from("subdir/file5.md")));
    assert_eq!(z3.followups.len(), 0);
    // So far for the things defined in the YAML-Metadata.
    // Let's check for targets of markdown hyperlinks
    assert_eq!(z1.links.len(), 0);
    assert_eq!(z2.links.len(), 1);
    assert!(z2.links.contains(&PathBuf::from("file1.md")));
    assert_eq!(z3.links.len(), 0);
}

#[test]
fn test_update_index_native() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    //let tmp_dir = setup_tmp_dir();
    //let rootdir = tmp_dir.path().join("examples/Zettelkasten").to_path_buf();
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let indexingmethod = IndexingMethod::Native;
    let ignorefile = PathBuf::from(".gitignore");
    let index = create_index(&indexingmethod, &rootdir, &ignorefile);
    assert!(index.is_ok());
    let mut index = index.unwrap();
    // Because of SystemTime isn't strictly monotonous, let's wait for ten ms
    sleep(Duration::from_millis(10));
    // rename one
    fs::rename(rootdir.join("file2.md"),
                rootdir.join("ren_file2.md"))
        .expect("Error deleting a temporary file.");
    // modifiy one
    fs::write(rootdir.join("file1.md"), "---
title:  'Modified File 1'
author: Stefan Thesing
keywords: [example1]
followups: [ren_file2.md]
lang: de
...

# Bla bla

Lorem ipsum.")
        .expect("Something went wrong overwriting the contents of Test 1");
    // create a new one
    let mut f6 = File::create(rootdir.join("file6.md"))
        .expect("Something went wrong with creating a temporary file for \
        testing.");
    writeln!(f6, "---
title:  'File 6'
author: Stefan Thesing
keywords: [example3]
followups: []
lang: de
...

# Bla bla

Lorem ipsum.").expect("Failed to write to file");
    // Now, let's update the index
    update_index(&mut index, &indexingmethod, &rootdir, &ignorefile)
        .expect("Received error from function 'update_index'!");
    assert_eq!(index.files.len(), 8);
    assert!(index.files.contains_key(&PathBuf::from("file1.md")));
    assert!(index.files.contains_key(&PathBuf::from("ren_file2.md")));
    assert!(index.files.contains_key(&PathBuf::from("file3.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file4.md")));
    assert!(index.files.contains_key(&PathBuf::from("subdir/file5.md")));
    assert!(index.files.contains_key(&PathBuf::from("file6.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/markdown-only.md")));
    assert!(index.files.contains_key(&PathBuf::from("onlies/pure-yaml.yaml")));

    // let's retrieve our two zettels and check the data.
    let z1 = index.get_zettel(PathBuf::from("file1.md"));
    assert!(z1.is_some());
    let z1 = z1.unwrap();
    let z2 = index.get_zettel(PathBuf::from("subdir/file4.md"));
    assert!(z2.is_some());
    let z2= z2.unwrap();
    let z3 = index.get_zettel(PathBuf::from("file6.md"));
    assert!(z3.is_some());
    let z3 = z3.unwrap();
    // Titles
    assert_eq!(z1.title, "Modified File 1");
    assert_eq!(z2.title, "File 4");
    assert_eq!(z3.title, "File 6");
    // Keywords
    assert_eq!(z1.keywords.len(), 1);
    assert!(z1.keywords.contains(&String::from("example1")));
    assert_eq!(z2.keywords.len(), 2);
    assert!(z2.keywords.contains(&String::from("example")));
    assert!(z2.keywords.contains(&String::from("fourth")));
    assert_eq!(z3.keywords.len(), 1);
    assert!(z3.keywords.contains(&String::from("example3")));
    // Followups
    assert_eq!(z1.followups.len(), 1);
    assert!(z1.followups.contains(&PathBuf::from("ren_file2.md")));
    assert_eq!(z2.followups.len(), 2);
    assert!(z2.followups.contains(&PathBuf::from("file1.md")));
    assert!(z2.followups.contains(&PathBuf::from("subdir/file5.md")));
    assert_eq!(z3.followups.len(), 0);
    // So far for the things defined in the YAML-Metadata.
    // Let's check for targets of markdown hyperlinks
    assert_eq!(z1.links.len(), 0);
    assert_eq!(z2.links.len(), 1);
    assert!(z2.links.contains(&PathBuf::from("file1.md")));
    assert_eq!(z3.links.len(), 0);
}

#[test]
fn test_normalize_path() {
    //Setup
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    let rootdir = dir.join("examples/Zettelkasten").to_path_buf();
    let file    = rootdir.join("file1.md");
    assert_ne!(file, PathBuf::from("file1.md"));
    
    // Test: Given an existing file, express its path relative to the rootdir
    assert_eq!(normalize_path(&rootdir, &file).unwrap(), 
               PathBuf::from("file1.md"));
}

#[test]
fn test_normalize_path_symlink() {
    //Setup
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    //Add a symlink to "examples/Zettelkasten"
    std::os::unix::fs::symlink(
        tmp_dir.path().join("examples/Zettelkasten/"),
        tmp_dir.path().join("symlinked"))
        .expect("Something went wrong with adding a symlink in the 
        temporary directory for this test.");
    
    // Test parameters
    let rootdir = tmp_dir.path().join("symlinked");
    let file    = tmp_dir.path().join("symlinked/subdir/file4.md");
    let real_rootdir = tmp_dir.path().join("examples/Zettelkasten/");
    // Compare to
    let relfile = PathBuf::from("subdir/file4.md");
    
    // Test: Given an existing file, express its path relative to the rootdir
    assert_eq!(normalize_path(&rootdir, &file).unwrap(), relfile);
    // Mix up symlink and real path
    assert_eq!(normalize_path(&real_rootdir, &file).unwrap(), relfile);
}

#[test]
fn test_normalize_link() {
    // Setup
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    // Test parameters
    let rootdir = tmp_dir.path().join("examples/Zettelkasten/");
    let file    = tmp_dir.path().join("examples/Zettelkasten/subdir/file4.md");
    let link    = PathBuf::from("../file1.md");
    
    // Given a file and a link, express that link relative to the rootdir
    assert_eq!(normalize_link(&rootdir, &file, &link).unwrap(),
                PathBuf::from("file1.md"));
}

#[test]
fn test_normalize_link_symlink() {
    // Setup
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    //Add a symlink to "examples/Zettelkasten"
    std::os::unix::fs::symlink(
        tmp_dir.path().join("examples/Zettelkasten/"),
        tmp_dir.path().join("symlinked"))
        .expect("Something went wrong with adding a symlink in the 
        temporary directory for this test.");
    
    // Test parameters
    let rootdir = tmp_dir.path().join("symlinked");
    let real_rootdir = tmp_dir.path().join("examples/Zettelkasten/");
    let file    = tmp_dir.path().join("symlinked/subdir/file4.md");
    let link    = PathBuf::from("../file1.md");
    
    // Given a file and a link, express that link relative to the rootdir
    assert_eq!(normalize_link(&rootdir, &file, &link).unwrap(),
                PathBuf::from("file1.md"));
    // Mix symlinked and real paths
    assert_eq!(normalize_link(&real_rootdir, &file, &link).unwrap(),
                PathBuf::from("file1.md"));
}

#[test]
fn test_parse_and_apply_markdown_links_grep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    // Test parameters
    let rootdir = tmp_dir.path().join("examples/Zettelkasten/");
    let f1      = rootdir.join("file1.md");
    let f2      = rootdir.join("subdir/file4.md");
    let mut index = Index::empty();
    // Two zettel stubs
    index.add_zettel("file1.md", Zettel::new("F1"));
    index.add_zettel("subdir/file4.md", Zettel::new("F2"));
    
    let files = vec![f1, f2];
    let indexingmethod = IndexingMethod::Grep;
    
    let result = parse_and_apply_markdown_links(&indexingmethod,
                                                &mut index,
                                                &rootdir, 
                                                files);
    assert!(result.is_ok());
    assert_eq!(index.files.len(), 2);
    let z2 = index.get_zettel("subdir/file4.md"); 
    assert!(z2.is_some());
    let z2 = z2.unwrap();
    // Let's see if the link to test1.md contained by test2 we got from 
    // setup_tmp_dir has been correctly written to links of the corresponding
    // zettel stub.
    assert!(z2.links.contains(&PathBuf::from("file1.md")));
}

#[test]
fn test_parse_and_apply_markdown_links_ripgrep() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    // Test parameters
    let rootdir = tmp_dir.path().join("examples/Zettelkasten/");
    let f1      = rootdir.join("file1.md");
    let f2      = rootdir.join("subdir/file4.md");
    let mut index = Index::empty();
    // Two zettel stubs
    index.add_zettel("file1.md", Zettel::new("F1"));
    index.add_zettel("subdir/file4.md", Zettel::new("F2"));
    
    let files = vec![f1, f2];
    let indexingmethod = IndexingMethod::RipGrep;
    
    let result = parse_and_apply_markdown_links(&indexingmethod,
                                                &mut index,
                                                &rootdir, 
                                                files);
    assert!(result.is_ok());
    assert_eq!(index.files.len(), 2);
    let z2 = index.get_zettel("subdir/file4.md"); 
    assert!(z2.is_some());
    let z2 = z2.unwrap();
    // Let's see if the link to test1.md contained by test2 we got from 
    // setup_tmp_dir has been correctly written to links of the corresponding
    // zettel stub.
    assert!(z2.links.contains(&PathBuf::from("file1.md")));
}

#[test]
fn test_parse_and_apply_markdown_links_native() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let dir = tmp_dir.path();
    generate_bare_examples(dir).expect("Failed to generate examples");
    // Test parameters
    let rootdir = tmp_dir.path().join("examples/Zettelkasten/");
    let f1      = rootdir.join("file1.md");
    let f2      = rootdir.join("subdir/file4.md");
    let mut index = Index::empty();
    // Two zettel stubs
    index.add_zettel("file1.md", Zettel::new("F1"));
    index.add_zettel("subdir/file4.md", Zettel::new("F2"));
    
    let files = vec![f1, f2];
    let indexingmethod = IndexingMethod::Native;
    
    let result = parse_and_apply_markdown_links(&indexingmethod,
                                                &mut index,
                                                &rootdir, 
                                                files);
    assert!(result.is_ok());
    assert_eq!(index.files.len(), 2);
    let z2 = index.get_zettel("subdir/file4.md"); 
    assert!(z2.is_some());
    let z2 = z2.unwrap();
    // Let's see if the link to test1.md contained by test2 we got from 
    // setup_tmp_dir has been correctly written to links of the corresponding
    // zettel stub.
    assert!(z2.links.contains(&PathBuf::from("file1.md")));
}

#[test]
fn test_add_textfiles_only() {
    let tmp_dir = tempdir().expect("Failed to setup temp dir");
    let rootdir = tmp_dir.path().to_path_buf();
    let mut index = Index::empty();
     //  prepare a textfile
    let text_file  = rootdir.join("text.md");
    let mut tf = fs::File::create(&text_file)
        .expect("Failed to create file.");
    writeln!(tf, "{}", "Foo").expect("Failed to write to file.");
    
     //  prepare an image file
    let image_file = rootdir.join("image.png");
    let width: u32 = 10;
    let height: u32 = 10;
    let mut non_text = image::ImageBuffer::new(width, height);
     //  color all pixels white
    for (_, _, pixel) in non_text.enumerate_pixels_mut() {
        *pixel = image::Rgb([255, 255, 255]);
    }
     //  Save it to file
    non_text.save(&image_file).unwrap();
    
     //  Add text file
    let result = add_textfiles_only(&mut index, &rootdir, &text_file);
    assert!(result.is_ok());
     //  Add image file
    let result = add_textfiles_only(&mut index, &rootdir, &image_file);
    assert!(result.is_ok());
     //  Both have happened without an error (except the one that is handled
     //  inside the function). But only the text file should be in there.
    assert_eq!(index.files.len(), 1);
    assert!(index.files.contains_key(&text_file));
}