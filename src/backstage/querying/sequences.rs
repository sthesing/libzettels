//Copyright (c) 2020-2022 Stefan Thesing
//
//This file is part of libzettels.
//
//libzettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//libzettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.
//! Module for querying the index.

// --------------------------------------------------------------------------

use std::path::{PathBuf, Path};
use backstage::index::Index;
use std::collections::HashSet;

// --------------------------------------------------------------------------

/// User configuration that defines which zettels should be considered as
/// the start of a sequence.
///
/// # What's a sequence?
/// One feature that distinguishes a Zettelkasten from other hypertext systems
/// is that it has *sequences* that are defined by the cascade of followup
/// zettels who have followups of their own. A sequence can be compared to 
/// a "line of thought" or an argumentation. For example, consider this
/// abstract description of a line of thought:
/// ```text, no_run
/// A problem
/// └── Considerations of the problem
///     └──  An approach to a solution
///         └──  Implications of this approach  
/// ```
/// If each step in this line of thought is a zettel, each designating the next
/// zettel as a followup, then the whole is a sequence.
/// 
/// In this example, the question which zettel is to be considered the start of
/// the sequence is trivial: it's the first one, the "root" of the sequence.
///
/// But sometimes one line of thought leads to another. It branches or 
/// specializes:
/// ```text, no_run
/// A problem
/// └── Considerations of the problem
///     └──  An approach to a solution
///         └──  Implications of this approach  
/// └── A different perspective of the problem
///     └──  A different approach to a solution
///         └──  Implications of this other approach
/// ```
/// One could consider the two followups of the sequence root to be the
/// beginning of new sequences.
/// 
/// # Sequences in libzettels
/// To identify a sequence, libzettel uses the path to the zettel that is the
/// start of said sequence. To do this, libzettels 
/// needs criteria to determine whether to consider a zettel the start of a
/// new sequence or not. That's the intended use of 
/// [`keywords`](struct.Zettel.html#structfield.keywords) in a 
/// [`Zettel`](struct.Zettel.html). However, different users might handle their
/// Zettelkasten differently and might apply keywords in different manners.
/// To reflect this, `SequenceStart` is a part of [Config](struct.Config.html).
/// 
/// A sequence root (i.e. a zettel that is not designated by another zettel
/// as a followup) is *always* considered the start of a sequence. How other
/// zettels are treated is set by the variants of `SequenceStart`.
#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum SequenceStart {
    /// No other zettels are considered to be the start of a sequence, only
    /// sequence roots (i.e. "orphans", zettels that are not designated by 
    /// another zettel as a followup).
    RootOnly,
    /// **Default**
    ///
    /// In addition to sequence roots, zettels are considered to be the start
    /// of a sequence if they have a
    /// [`keyword`](struct.Zettel.html#structfield.keywords). This is the
    /// default behaviour.
    Keyword,
    /// In addition to sequence roots, zettels are considered to be the start
    /// of a sequence if they are not the only followup of their predecessor
    /// in a sequence, thus forming a "fork" or branch.
    Fork,
    /// In addition to sequence roots, zettels are considered to be the start
    /// of a sequence if they are not the only followup of their predecessor
    /// in a sequence AND have a
    /// [`keyword`](struct.Zettel.html#structfield.keywords).
    KeywordAndFork,
    /// In addition to sequence roots, zettels are considered to be the start
    /// of a sequence if they are not the only followup of their predecessor
    /// in a sequence OR have a
    /// [`keyword`](struct.Zettel.html#structfield.keywords).
    KeywordOrFork,
}

/// Returns a set of zettels which list at least one of the zettels 
/// specified by `scope` as a followup.
pub fn parents_of_scope(index: &Index,
                                        scope: Vec<PathBuf>)
                                        -> HashSet<PathBuf> {
    let mut parents = HashSet::new();
    for zettel in scope {
        let mut zettel_parents = parents_of_zettel(&index, &zettel);
        
        // Merge with parents
        
        // Apparently, this:
        for zp in zettel_parents.drain() {
            parents.insert(zp);
        }
        // is more efficient than this:
        //parents = parents.union(&zettel_parents)
                            //.map(|p| p.clone())
                            //.collect();
    }
    parents
}

/// Returns a list of zettels which list the zettel specified by `key` as
/// a followup.
fn parents_of_zettel<P: AsRef<Path>>(index: &Index, 
                                         key: P)
                                         -> HashSet<PathBuf> {
    let mut parents = HashSet::new();
    let key = key.as_ref().to_path_buf();
    for (path, zettel) in &index.files {
        if zettel.followups.contains(&key) {
            parents.insert(path.to_path_buf());
        }
    }
    parents
}


/// Returns a list of zettels that are part of all sequences of which the 
/// zettels specified by `scope` are also a part of.
pub fn sequence_tree(index: &Index,
                                     scope: &HashSet<PathBuf>)
                                     -> HashSet<PathBuf> {
    let mut sequence_tree = HashSet::new();
    for zettel in scope {
        let stn_of_zettel = sequence_tree_of_zettel(&index, zettel);
        for z in stn_of_zettel {
            sequence_tree.insert(z);
        }
    }
    sequence_tree
}


/// Returns a list of zettels that are part of all sequences of which the 
/// zettel specified by `key` is also a part of.
fn sequence_tree_of_zettel<P: AsRef<Path>>(index: &Index,
                                                    key: P)
                                                    -> HashSet<PathBuf> {
    let key = key.as_ref();
    let mut ad = ancestors_and_descendants(&index, &key);
    // Add the Zettel itself
    ad.insert(key.to_path_buf());
    ad
}


/// Returns a list of zettels that are part of all sequences of which the 
/// zettels specified by `scope` are also a part of, as well as related sequences.
pub fn sequence_tree_whole(index: &Index, scope: &HashSet<PathBuf>)
                                    -> HashSet<PathBuf> {
    let mut sequence_tree = HashSet::new();
    for zettel in scope {
        let wst_of_zettel = sequence_tree_whole_of_zettel(&index, zettel);
        for z in wst_of_zettel {
            sequence_tree.insert(z);
        }
    }
    sequence_tree
}

/// Returns a list of zettels that are part of all sequences of which the 
/// zettel specified by `key` is also a part of, as well as related sequences.
fn sequence_tree_whole_of_zettel<P: AsRef<Path>>(index: &Index,
                                                     key: P)
                                                     -> HashSet<PathBuf> {
    let mut sequence_tree = HashSet::new();                                                
    // We want everything related to our zettel, so we ask for the
    // zettels sequences this zettel is involved in, but only for root 
    // sequences
    let root_sequences = sequences_of_zettel(&index, 
                                             key, 
                                             &SequenceStart::RootOnly);
    for sequence in root_sequences {
        let sq_zettels = zettels_of_sequence(&index, sequence);
        for z in sq_zettels {
            sequence_tree.insert(z);
        }
    }
    sequence_tree
}

/// Returns a list of all the sequences the Zettels specified by `scope` is
/// a part of.
pub fn sequences(index: &Index,
                          scope: &HashSet<PathBuf>,
                          cfg_sequence_start: &SequenceStart)
                          -> HashSet<PathBuf> {
    let mut sequences = HashSet::new();
    for zettel in scope {
        let zettel_sequences = sequences_of_zettel(&index, 
                                                   &zettel, 
                                                   cfg_sequence_start);
        for zs in zettel_sequences {
            sequences.insert(zs);
        }
    }
    sequences
}

/// Returns a list of all the squences the Zettel specified by `key` is
/// a part of.
fn sequences_of_zettel<P: AsRef<Path>>(index: &Index,
                                            key: P,
                                            cfg_sequence_start: &SequenceStart) 
                                            -> HashSet<PathBuf> {
    let key = key.as_ref();
    let mut sequences = HashSet::new();
    let (check, parents) = check_sequence_start(&index, 
                                             &key, 
                                             &cfg_sequence_start);
    
    // If it's the start of a sequence, add it to our return value
    if check {
        sequences.insert(key.to_path_buf());
    }
    
    // If it has parents, we need to add their containing sequences, too
    for parent in parents {
        // We do this recursively
        let parent_sequences = sequences_of_zettel(&index, 
                                                    parent, 
                                                    &cfg_sequence_start);
        // Merge
        for parent_sequence in parent_sequences {
            sequences.insert(parent_sequence);
        }
    }
    // if not, this is an orphan and we can return our value
    sequences
}

/// Using the Zettel specified by `key` as an identifier for a sequence, this 
/// function returns a list of all the zettels that belong to that sequence.
pub fn zettels_of_sequence<P: AsRef<Path>>(index: &Index,
                                               key: P)
                                               -> HashSet<PathBuf> {
    let key = key.as_ref().to_path_buf();
    let mut zettels = HashSet::new();
    zettels.insert(key.clone());
    if let Some(z) = &index.get_zettel(&key) {
        for f in &z.followups {
            let rest_sequence = zettels_of_sequence(&index,
                                                        &f);
            for rest in rest_sequence {
                zettels.insert(rest);
            }
        }
    }
    zettels
}

// --------------------------------------------------------------------------
// Other private functions
// --------------------------------------------------------------------------


/// Returns a list of both ancestors and descendants of the specified Zettel.
/// See the functions `ancestors_of_zettel` and `descendants_of_zettel` for
/// details.
/// The list is returned as a HashSet, so all family members are listed only
/// once.
fn ancestors_and_descendants<P: AsRef<Path>>(index: &Index, 
                                                 key: P) 
                                                 -> HashSet<PathBuf> {
    let key = key.as_ref();
    let mut ad = HashSet::new();
    let ancestors = ancestors_of_zettel(&index, &key);
    for a in ancestors {
        ad.insert(a);
    }
    let descendants = descendants_of_zettel(&index, &key);
    for d in descendants {
        ad.insert(d);
    }
    ad
}

/// Returns a list of Zettels that have the specified Zettel as a followup, as
/// well as the ancestors of those Zettels.
/// The list is returned as a HashSet, so each ancestor is only listed once.
fn ancestors_of_zettel<P: AsRef<Path>>(index: &Index,
                             key: P)
                             -> HashSet<PathBuf> {
    let key = key.as_ref();
    let mut ancestors = HashSet::new();
    let parents = parents_of_zettel(&index, key);
    for parent in parents {
        // Add the parent itself
        // To safeguard against circle parentages, we actually read the
        // return value of the insert method, which is `true` if the
        // parent was not yet present in ancestors. We only do the 
        // recursive rest in that case.
        if ancestors.insert(parent.clone()) {
            // Add its ancestors, recursively
            let parent_ancestors = ancestors_of_zettel(&index, parent);
            for pa in parent_ancestors {
                ancestors.insert(pa);
            }
        }
    }
    ancestors
}

/// Returns a list of followups of the specified Zettels, as well as the 
/// followups of those Zettels.
/// The list is returned as a HashSet, so each descendant is only listed once.
fn descendants_of_zettel<P: AsRef<Path>>(index: &Index,
                                         key: P)
                                         -> HashSet<PathBuf> {
    let key = key.as_ref();
    let mut descendants = HashSet::new();
    if let Some(zettel) = &index.get_zettel(&key) {
        for followup in &zettel.followups {
            // Add the followup itself
            // To safeguard against circle parentages, we actually read the
            // return value of the insert method, which is `true` if the
            // followup was not yet present in `descendants`. We only do the 
            // recursive rest in that case.
            if descendants.insert(followup.clone()) {
                let followup_descendants = descendants_of_zettel(&index, followup);
                for fd in followup_descendants {
                    descendants.insert(fd);
                }
            }
        }
    }    
    descendants
}

/// Returns a list of followups of all the zettels contained in `parents`.
fn children_of_parents(index: &Index, 
                       parents: &HashSet<PathBuf>) 
                       -> HashSet<PathBuf> {
    let mut children = HashSet::new();
    for p in parents {
        // make sure the parent is actually in the index
        if let Some(parent) = &index.get_zettel(&p) {
            for f in &parent.followups {
                children.insert(f.clone());
            }
        }
    }
    children
}

/// Returns the zettel(s) which list the zettel specified by `key` as a 
/// followup as well as their other followups.
fn parents_and_siblings<P: AsRef<Path>>(index: &Index,
                                            key: P)
                                            -> (HashSet<PathBuf>, HashSet<PathBuf>) {
    let parents = parents_of_zettel(&index, &key);
    let key = key.as_ref().to_path_buf();
    if parents.is_empty() {
        return (parents, HashSet::new()); // No parents, ergo no siblings
    } else {
        let mut children = children_of_parents(index, &parents);
        // we only want the siblings, not the zettel defined by key, itself
        children.remove(&key);
        return (parents, children);
    }
}

/// Returns wether or not a Zettel is – according to the user's settings – 
/// considered to be the start of a sequence.  
/// The return value is a tuple, containing
/// 1. A `bool`ean whether or not the Zettel is a start of a sequence
/// 2. The list of the Zettel's parents as a `HashSet`
fn check_sequence_start<P: AsRef<Path>>(index: &Index, 
                                         key: P,
                                         cfg_sequence_start: &SequenceStart) 
                                         -> (bool, HashSet<PathBuf>) {
    let (parents, siblings) = parents_and_siblings(&index, &key);
    if parents.is_empty() {
        // This Zettel is a sequence root (doesn't have parents).
        return (true, parents);  
    } else {
        if let Some(zettel) = index.get_zettel(key) {
            match cfg_sequence_start {
                SequenceStart::RootOnly => (), // Roots are covered, above.
                SequenceStart::Keyword    => {
                    if !zettel.keywords.is_empty() {
                        return (true, parents);
                    }
                },
                SequenceStart::Fork       => {
                    if !siblings.is_empty() {
                        return (true, parents);
                    }
                },
                SequenceStart::KeywordAndFork => {
                    if !zettel.keywords.is_empty() && !siblings.is_empty() {
                        return (true, parents);
                    }
                },
                SequenceStart::KeywordOrFork => {
                    if !zettel.keywords.is_empty() || !siblings.is_empty() {
                        return (true, parents);
                    }
                },
            }
        }
    }
    // Still here? Then it's no sqeuence start and it has parents.
    (false, parents)
}

// --------------------------------------------------------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use Zettel;
    
    #[test]
    fn test_parents_of_zettel() {
        let mut index = Index::empty();
        let child = Zettel::new("Child");
        index.add_zettel("child.md", child);
        let mut parent = Zettel::new("Parent");
        parent.add_followup("child.md");
        index.add_zettel("parent.md", parent);
        let mut parent2 = Zettel::new("Other Parent");
        parent2.add_followup("child.md");
        index.add_zettel("parent2.md", parent2);
        let unrelated = Zettel::new("Unrelated");
        index.add_zettel("unrelated.md", unrelated);
        
        assert_eq!(parents_of_zettel(&index, "unrelated.md").len(), 0);
        let parents = parents_of_zettel(&index, "child.md");
        assert_eq!(parents.len(), 2);
        assert!(parents.contains(&PathBuf::from("parent.md")));
        assert!(parents.contains(&PathBuf::from("parent2.md")));
    }
    
     #[test]
    fn test_parents_of_scope() {
        let mut index = Index::empty();
        let child = Zettel::new("Child");
        index.add_zettel("child.md", child);
        let mut parent = Zettel::new("Parent");
        parent.add_followup("child.md");
        index.add_zettel("parent.md", parent);
        let mut parent2 = Zettel::new("Other Parent");
        parent2.add_followup("child.md");
        index.add_zettel("parent2.md", parent2);
        
        let otherchild = Zettel::new("Other Child");
        index.add_zettel("otherchild.md", otherchild);
        let mut parent3 = Zettel::new("Parent3");
        parent3.add_followup("otherchild.md");
        index.add_zettel("parent3.md", parent3);
        
        let unrelated = Zettel::new("Unrelated");
        index.add_zettel("unrelated.md", unrelated);
        
        let scope = vec![PathBuf::from("child.md"), PathBuf::from("otherchild.md")];
        let parents = parents_of_scope(&index, scope);
                
        assert!(parents.contains(&PathBuf::from("parent.md")));
        assert!(parents.contains(&PathBuf::from("parent2.md")));
        assert!(parents.contains(&PathBuf::from("parent3.md")));
    }
    
    
    #[test]
    fn test_parents_and_siblings() {
        let mut index = Index::empty();
        let child_zettel = Zettel::new("A Child");
        index.add_zettel("child.md", child_zettel);
        let mut our_zettel = Zettel::new("Our Zettel");
        our_zettel.add_followup("child.md");
        index.add_zettel("our_zettel.md", our_zettel);
        let s1 = Zettel::new("1st Sibling");
        index.add_zettel("s1.md", s1);
        let s2 = Zettel::new("2nd Sibling");
        index.add_zettel("s2.md", s2);
        let mut parent = Zettel::new("Parent");
        parent.add_followup("our_zettel.md");
        parent.add_followup("s1.md");
        parent.add_followup("s2.md");
        index.add_zettel("parent.md", parent);
        
        // First, the obvious
        let (p, s) = parents_and_siblings(&index, "our_zettel.md");
        assert_eq!(p.len(), 1);
        assert!(p.contains(&PathBuf::from("parent.md")));
        assert_eq!(s.len(), 2);
        assert!(s.contains(&PathBuf::from("s1.md")));
        assert!(s.contains(&PathBuf::from("s2.md")));
        
        // Now, let's see if our parent has parents and siblings. Spoiler: it doesn't
        let (p, s) = parents_and_siblings(&index, "parent.md");
        assert!(p.is_empty());
        assert!(s.is_empty());
        
        // And the child?
        let (p, s) = parents_and_siblings(&index, "child.md");
        assert_eq!(p.len(), 1);
        assert!(p.contains(&PathBuf::from("our_zettel.md")));
        assert_eq!(s.len(), 0);
    }
    
    fn sequence_test_index() -> Index {
        let mut index = Index::empty();
        let mut f1 = Zettel::new("File 1");
        let mut f2 = Zettel::new("File 2");
        let mut f3 = Zettel::new("File 3");
        let mut f4 = Zettel::new("File 4");
        let f5 = Zettel::new("File 5");
        let mut f6 = Zettel::new("File 6");
        let mut f7 = Zettel::new("File 7");
        
        f1.add_keyword("example");
        f3.add_keyword("foo");
        f4.add_keyword("bar");
        f7.add_keyword("baz");
        
        f1.add_followup("file2.md");
        f2.add_followup("file3.md");
        f2.add_followup("file4.md");
        f3.add_followup("file5.md");
        f4.add_followup("file5.md"); // double parentage for this test
        f4.add_followup("file6.md");
        f6.add_followup("file7.md");
        
        index.add_zettel("file1.md", f1);
        index.add_zettel("file2.md", f2);
        index.add_zettel("file3.md", f3);
        index.add_zettel("file4.md", f4);
        index.add_zettel("file5.md", f5);
        index.add_zettel("file6.md", f6);
        index.add_zettel("file7.md", f7);
        index
    }
    
    #[test]
    fn test_sequence_start_orphan_only() {
        let index = sequence_test_index();
        
        let cfg_ss = SequenceStart::RootOnly;
        let (check, parents) = check_sequence_start(&index, "file1.md", &cfg_ss);
        assert_eq!(check, true);
        assert!(parents.is_empty());
        
        let (check, parents) = check_sequence_start(&index, "file2.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file1.md")));
        
        let (check, parents) = check_sequence_start(&index, "file3.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file2.md")));
        
        let (check, parents) = check_sequence_start(&index, "file4.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file2.md")));
        
        let (check, parents) = check_sequence_start(&index, "file5.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 2); // double parentage
        assert!(parents.contains(&PathBuf::from("file3.md")));
        assert!(parents.contains(&PathBuf::from("file4.md")));
        
        let (check, parents) = check_sequence_start(&index, "file6.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file4.md")));
        
        let (check, parents) = check_sequence_start(&index, "file7.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file6.md")));
    }
    
    #[test]
    fn test_sequence_start_keyword() {
        let index = sequence_test_index();
        
        let cfg_ss = SequenceStart::Keyword;
        let (check, parents) = check_sequence_start(&index, "file1.md", &cfg_ss);
        assert_eq!(check, true);
        assert!(parents.is_empty());
        
        let (check, parents) = check_sequence_start(&index, "file2.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file1.md")));
        
        let (check, parents) = check_sequence_start(&index, "file3.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file2.md")));
        
        let (check, parents) = check_sequence_start(&index, "file4.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file2.md")));
        
        let (check, parents) = check_sequence_start(&index, "file5.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 2); // double parentage
        assert!(parents.contains(&PathBuf::from("file3.md")));
        assert!(parents.contains(&PathBuf::from("file4.md")));
        
        let (check, parents) = check_sequence_start(&index, "file6.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file4.md")));
        
        let (check, parents) = check_sequence_start(&index, "file7.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file6.md")));
    }
    
    #[test]
    fn test_sequence_start_fork() {
        let index = sequence_test_index();
        
        let cfg_ss = SequenceStart::Fork;
        let (check, parents) = check_sequence_start(&index, "file1.md", &cfg_ss);
        assert_eq!(check, true);
        assert!(parents.is_empty());
        
        let (check, parents) = check_sequence_start(&index, "file2.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file1.md")));
        
        let (check, parents) = check_sequence_start(&index, "file3.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file2.md")));
        
        let (check, parents) = check_sequence_start(&index, "file4.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file2.md")));
        
        let (check, parents) = check_sequence_start(&index, "file5.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 2); // double parentage
        assert!(parents.contains(&PathBuf::from("file3.md")));
        assert!(parents.contains(&PathBuf::from("file4.md")));
        
        let (check, parents) = check_sequence_start(&index, "file6.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file4.md")));
        
        let (check, parents) = check_sequence_start(&index, "file7.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file6.md")));
    }
    
    #[test]
    fn test_sequence_start_keyword_and_fork() {
        let index = sequence_test_index();
        
        let cfg_ss = SequenceStart::KeywordAndFork;
        let (check, parents) = check_sequence_start(&index, "file1.md", &cfg_ss);
        assert_eq!(check, true);
        assert!(parents.is_empty());
        
        let (check, parents) = check_sequence_start(&index, "file2.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file1.md")));
        
        let (check, parents) = check_sequence_start(&index, "file3.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file2.md")));
        
        let (check, parents) = check_sequence_start(&index, "file4.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file2.md")));
        
        let (check, parents) = check_sequence_start(&index, "file5.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 2); // double parentage
        assert!(parents.contains(&PathBuf::from("file3.md")));
        assert!(parents.contains(&PathBuf::from("file4.md")));
        
        let (check, parents) = check_sequence_start(&index, "file6.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file4.md")));
        
        let (check, parents) = check_sequence_start(&index, "file7.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file6.md")));
    }
    
    #[test]
    fn test_sequence_start_keyword_or_fork() {
        let index = sequence_test_index();
        
        let cfg_ss = SequenceStart::KeywordOrFork;
        let (check, parents) = check_sequence_start(&index, "file1.md", &cfg_ss);
        assert_eq!(check, true);
        assert!(parents.is_empty());
        
        let (check, parents) = check_sequence_start(&index, "file2.md", &cfg_ss);
        assert_eq!(check, false);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file1.md")));
        
        let (check, parents) = check_sequence_start(&index, "file3.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file2.md")));
        
        let (check, parents) = check_sequence_start(&index, "file4.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file2.md")));
        
        let (check, parents) = check_sequence_start(&index, "file5.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 2); // double parentage
        assert!(parents.contains(&PathBuf::from("file3.md")));
        assert!(parents.contains(&PathBuf::from("file4.md")));
        
        let (check, parents) = check_sequence_start(&index, "file6.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file4.md")));
        
        let (check, parents) = check_sequence_start(&index, "file7.md", &cfg_ss);
        assert_eq!(check, true);
        assert_eq!(parents.len(), 1);
        assert!(parents.contains(&PathBuf::from("file6.md")));
    }
    
    #[test]
    fn test_sequences_of_zettel_orphan_only() {
        let index = sequence_test_index();
        let cfg_ss = SequenceStart::RootOnly;
        
        // We test for file7.md. Most potential sequences
        // And for file5.md, because of the double parentage
        
        let sequences = sequences_of_zettel(&index, "file7.md", &cfg_ss);
        assert_eq!(sequences.len(), 1);
        assert!(sequences.contains(&PathBuf::from("file1.md")));
        
        let sequences = sequences_of_zettel(&index, "file5.md", &cfg_ss);
        assert_eq!(sequences.len(), 1);
        assert!(sequences.contains(&PathBuf::from("file1.md")));
    }
    
    #[test]
    fn test_sequences_of_zettel_keyword() {
        let index = sequence_test_index();
        let cfg_ss = SequenceStart::Keyword;
        
        // We test for file7.md. Most potential sequences
        // And for file5.md, because of the double parentage
        
        let sequences = sequences_of_zettel(&index, "file7.md", &cfg_ss);
        assert_eq!(sequences.len(), 3);
        assert!(sequences.contains(&PathBuf::from("file1.md")));
        assert!(sequences.contains(&PathBuf::from("file4.md")));
        assert!(sequences.contains(&PathBuf::from("file7.md")));
        
        let sequences = sequences_of_zettel(&index, "file5.md", &cfg_ss);
        assert_eq!(sequences.len(), 3);
        assert!(sequences.contains(&PathBuf::from("file1.md")));
        assert!(sequences.contains(&PathBuf::from("file3.md")));
        assert!(sequences.contains(&PathBuf::from("file4.md")));
    }
    
    #[test]
    fn test_sequences_of_zettel_fork() {
        let index = sequence_test_index();
        let cfg_ss = SequenceStart::Fork;
        
        // We test for file7.md. Most potential sequences
        // And for file5.md, because of the double parentage
        
        let sequences = sequences_of_zettel(&index, "file7.md", &cfg_ss);
        assert_eq!(sequences.len(), 3);
        assert!(sequences.contains(&PathBuf::from("file1.md")));
        assert!(sequences.contains(&PathBuf::from("file4.md")));
        assert!(sequences.contains(&PathBuf::from("file6.md")));
        
        let sequences = sequences_of_zettel(&index, "file5.md", &cfg_ss);
        assert_eq!(sequences.len(), 4);
        assert!(sequences.contains(&PathBuf::from("file1.md")));
        assert!(sequences.contains(&PathBuf::from("file3.md")));
        assert!(sequences.contains(&PathBuf::from("file4.md")));
        assert!(sequences.contains(&PathBuf::from("file5.md")));
    }
    
    #[test]
    fn test_sequences_of_zettel_keyword_and_fork() {
        let index = sequence_test_index();
        let cfg_ss = SequenceStart::KeywordAndFork;
        
        // We test for file7.md. Most potential sequences
        // And for file5.md, because of the double parentage
        
        let sequences = sequences_of_zettel(&index, "file7.md", &cfg_ss);
        assert_eq!(sequences.len(), 2);
        assert!(sequences.contains(&PathBuf::from("file1.md")));
        assert!(sequences.contains(&PathBuf::from("file4.md")));
        
        let sequences = sequences_of_zettel(&index, "file5.md", &cfg_ss);
        assert_eq!(sequences.len(), 3);
        assert!(sequences.contains(&PathBuf::from("file1.md")));
        assert!(sequences.contains(&PathBuf::from("file3.md")));
        assert!(sequences.contains(&PathBuf::from("file4.md")));
    }
    
    #[test]
    fn test_sequences_of_zettel_keyword_or_fork() {
        let index = sequence_test_index();
        let cfg_ss = SequenceStart::KeywordOrFork;
        
        // We test for file7.md. Most potential sequences
        // And for file5.md, because of the double parentage
        
        let sequences = sequences_of_zettel(&index, "file7.md", &cfg_ss);
        assert_eq!(sequences.len(), 4);
        assert!(sequences.contains(&PathBuf::from("file1.md")));
        assert!(sequences.contains(&PathBuf::from("file4.md")));
        assert!(sequences.contains(&PathBuf::from("file6.md")));
        assert!(sequences.contains(&PathBuf::from("file7.md")));
        
        let sequences = sequences_of_zettel(&index, "file5.md", &cfg_ss);
        assert_eq!(sequences.len(), 4);
        assert!(sequences.contains(&PathBuf::from("file1.md")));
        assert!(sequences.contains(&PathBuf::from("file3.md")));
        assert!(sequences.contains(&PathBuf::from("file4.md")));
        assert!(sequences.contains(&PathBuf::from("file5.md")));
    }
    
    #[test]
    fn test_zettels_of_sequence() {
        let index = sequence_test_index();
        
        let zettels = zettels_of_sequence(&index, "file1.md");
        assert_eq!(zettels.len(), 7);
        assert!(zettels.contains(&PathBuf::from("file1.md")));
        assert!(zettels.contains(&PathBuf::from("file2.md")));
        assert!(zettels.contains(&PathBuf::from("file3.md")));
        assert!(zettels.contains(&PathBuf::from("file4.md")));
        assert!(zettels.contains(&PathBuf::from("file5.md")));
        assert!(zettels.contains(&PathBuf::from("file6.md")));
        assert!(zettels.contains(&PathBuf::from("file7.md")));
        
        let zettels = zettels_of_sequence(&index, "file2.md");
        assert_eq!(zettels.len(), 6);
        assert!(zettels.contains(&PathBuf::from("file2.md")));
        assert!(zettels.contains(&PathBuf::from("file3.md")));
        assert!(zettels.contains(&PathBuf::from("file4.md")));
        assert!(zettels.contains(&PathBuf::from("file5.md")));
        assert!(zettels.contains(&PathBuf::from("file6.md")));
        assert!(zettels.contains(&PathBuf::from("file7.md")));
        
        let zettels = zettels_of_sequence(&index, "file3.md");
        assert_eq!(zettels.len(), 2);
        assert!(zettels.contains(&PathBuf::from("file3.md")));
        assert!(zettels.contains(&PathBuf::from("file5.md")));
        
        let zettels = zettels_of_sequence(&index, "file4.md");
        assert_eq!(zettels.len(), 4);
        assert!(zettels.contains(&PathBuf::from("file4.md")));
        assert!(zettels.contains(&PathBuf::from("file5.md")));
        assert!(zettels.contains(&PathBuf::from("file6.md")));
        assert!(zettels.contains(&PathBuf::from("file7.md")));
        
        let zettels = zettels_of_sequence(&index, "file5.md");
        assert_eq!(zettels.len(), 1);    
        assert!(zettels.contains(&PathBuf::from("file5.md")));    
        
        let zettels = zettels_of_sequence(&index, "file6.md");
        assert_eq!(zettels.len(), 2);    
        assert!(zettels.contains(&PathBuf::from("file6.md")));
        assert!(zettels.contains(&PathBuf::from("file7.md")));
        
        let zettels = zettels_of_sequence(&index, "file7.md");
        assert_eq!(zettels.len(), 1);    
        assert!(zettels.contains(&PathBuf::from("file7.md")));    
    }
    
    #[test]
    fn test_ancestors_of_zettel() {
        let index = sequence_test_index();
        
        let ancestors = ancestors_of_zettel(&index, "file4.md");
        assert_eq!(ancestors.len(), 2);
        assert!(ancestors.contains(&PathBuf::from("file1.md")));
        assert!(ancestors.contains(&PathBuf::from("file2.md")));
        
        let ancestors = ancestors_of_zettel(&index, "file5.md");
        assert_eq!(ancestors.len(), 4);
        assert!(ancestors.contains(&PathBuf::from("file1.md")));
        assert!(ancestors.contains(&PathBuf::from("file2.md")));
        assert!(ancestors.contains(&PathBuf::from("file3.md")));
        assert!(ancestors.contains(&PathBuf::from("file4.md")));
        
        
        let ancestors = ancestors_of_zettel(&index, "file7.md");
        assert_eq!(ancestors.len(), 4);
        assert!(ancestors.contains(&PathBuf::from("file1.md")));
        assert!(ancestors.contains(&PathBuf::from("file2.md")));
        assert!(ancestors.contains(&PathBuf::from("file4.md")));
        assert!(ancestors.contains(&PathBuf::from("file6.md")));
    }
    
    #[test]
    fn test_descendants_of_zettel() {
        let index = sequence_test_index();
        
        let descendants = descendants_of_zettel(&index, "file3.md");
        assert_eq!(descendants.len(), 1);
        assert!(descendants.contains(&PathBuf::from("file5.md")));
        
        let descendants = descendants_of_zettel(&index, "file2.md");
        assert_eq!(descendants.len(), 5);
        assert!(descendants.contains(&PathBuf::from("file3.md")));
        assert!(descendants.contains(&PathBuf::from("file4.md")));
        assert!(descendants.contains(&PathBuf::from("file5.md")));
        assert!(descendants.contains(&PathBuf::from("file6.md")));
        assert!(descendants.contains(&PathBuf::from("file7.md")));
    }
    
    #[test]
    fn test_ancestors_and_descendants() {
        let index = sequence_test_index();
        
        let ad = ancestors_and_descendants(&index, "file4.md");
        assert_eq!(ad.len(), 5);
        assert!(ad.contains(&PathBuf::from("file1.md")));
        assert!(ad.contains(&PathBuf::from("file2.md")));
        assert!(ad.contains(&PathBuf::from("file5.md")));
        assert!(ad.contains(&PathBuf::from("file6.md")));
        assert!(ad.contains(&PathBuf::from("file7.md")));
        
        let ad = ancestors_and_descendants(&index, "file5.md");
        assert_eq!(ad.len(), 4);
        assert!(ad.contains(&PathBuf::from("file1.md")));
        assert!(ad.contains(&PathBuf::from("file2.md")));
        assert!(ad.contains(&PathBuf::from("file3.md")));
        assert!(ad.contains(&PathBuf::from("file4.md")));
        
        let ad = ancestors_and_descendants(&index, "file6.md");
        assert_eq!(ad.len(), 4);
        assert!(ad.contains(&PathBuf::from("file1.md")));
        assert!(ad.contains(&PathBuf::from("file2.md")));
        assert!(ad.contains(&PathBuf::from("file4.md")));
        assert!(ad.contains(&PathBuf::from("file7.md")));
    }
    
    #[test]
    fn test_sequence_tree_of_zettel() {
        let index = sequence_test_index();
        
        let stn = sequence_tree_of_zettel(&index, "file4.md");
        assert_eq!(stn.len(), 6);
        assert!(stn.contains(&PathBuf::from("file1.md")));
        assert!(stn.contains(&PathBuf::from("file2.md")));
        assert!(stn.contains(&PathBuf::from("file4.md")));
        assert!(stn.contains(&PathBuf::from("file5.md")));
        assert!(stn.contains(&PathBuf::from("file6.md")));
        assert!(stn.contains(&PathBuf::from("file7.md")));
    }
    
    #[test]
    fn test_sequence_tree() {
        let index = sequence_test_index();
        let mut scope = HashSet::new();
        scope.insert(PathBuf::from("file4.md"));
        scope.insert(PathBuf::from("file6.md"));
        
        let stn = sequence_tree(&index, &scope);
        assert_eq!(stn.len(), 6);
        assert!(stn.contains(&PathBuf::from("file1.md")));
        assert!(stn.contains(&PathBuf::from("file2.md")));
        assert!(stn.contains(&PathBuf::from("file4.md")));
        assert!(stn.contains(&PathBuf::from("file5.md")));
        assert!(stn.contains(&PathBuf::from("file6.md")));
        assert!(stn.contains(&PathBuf::from("file7.md")));
    }
    
    #[test]
    fn test_sequence_tree_whole_of_zettel() {
        let index = sequence_test_index();
        let wst = sequence_tree_whole_of_zettel(&index, "file4.md");
        println!("{:#?}", wst);
        assert_eq!(wst.len(), 7);
        assert!(wst.contains(&PathBuf::from("file1.md")));
        assert!(wst.contains(&PathBuf::from("file2.md")));
        assert!(wst.contains(&PathBuf::from("file3.md")));
        assert!(wst.contains(&PathBuf::from("file4.md")));
        assert!(wst.contains(&PathBuf::from("file5.md")));
        assert!(wst.contains(&PathBuf::from("file6.md")));
        assert!(wst.contains(&PathBuf::from("file7.md")));
    }
    
    #[test]
    fn test_sequence_tree_whole() {
        let index = sequence_test_index();
        let mut scope = HashSet::new();
        scope.insert(PathBuf::from("file4.md"));
        scope.insert(PathBuf::from("file6.md"));
        let wst = sequence_tree_whole(&index, &scope);
        println!("{:#?}", wst);
        assert_eq!(wst.len(), 7);
        assert!(wst.contains(&PathBuf::from("file1.md")));
        assert!(wst.contains(&PathBuf::from("file2.md")));
        assert!(wst.contains(&PathBuf::from("file3.md")));
        assert!(wst.contains(&PathBuf::from("file4.md")));
        assert!(wst.contains(&PathBuf::from("file5.md")));
        assert!(wst.contains(&PathBuf::from("file6.md")));
        assert!(wst.contains(&PathBuf::from("file7.md")));
    }
}
