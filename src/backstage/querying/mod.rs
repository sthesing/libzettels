//Copyright (c) 2020-2022 Stefan Thesing
//
//This file is part of libzettels.
//
//libzettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//libzettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.

//! Module for querying the index.

// --------------------------------------------------------------------------

use std::path::{PathBuf};
use std::collections::{HashSet, BTreeMap};
use backstage::index::Index;
use backstage::zettel::Zettel;
pub mod sequences;

// --------------------------------------------------------------------------
// Search
// --------------------------------------------------------------------------
// Keywords

/// Takes a reference to the index and returns a list of all used keywords
/// (as a HashSet).
/// Called by a wrapper function of Index. See there for details.
pub fn get_keywords(index: &Index) -> HashSet<String> {
    let mut keywords = HashSet::new();
    for (_, zettel) in &index.files {
        for keyword in &zettel.keywords {
            keywords.insert(keyword.to_string());
        }
    }
    keywords
}

/// Takes a reference to the index and returns a list containing the number
/// of occurences of every keywords (as a BTreeMap).
/// Called by a wrapper function of Index. See there for details.
pub fn count_keywords(index: &Index) -> BTreeMap<String, u32> {
    let mut counted_keywords = BTreeMap::new();
    for (_, zettel) in &index.files {
        for keyword in &zettel.keywords {
            if let Some(count) = counted_keywords.get(keyword) {
                let new_count = count + 1;
                counted_keywords.insert(keyword.to_string(), new_count);
            } else {
                counted_keywords.insert(keyword.to_string(), 1);
            }
        }
    }
    counted_keywords
}

/// Takes a reference to the index and a vector of Strings with keywords. 
/// Returns a list of all zettels that have one or – if `all` is true – 
/// all these keywords.
/// Called by a wrapper function of Index. See there for details.
pub fn search_index_for_keywords(index: &Index, 
                                searched_keywords: Vec<String>,
                                all: bool) -> HashSet<PathBuf> {
    let mut matching_zettels = HashSet::new();
    for (key, zettel) in &index.files {
        if search_zettel_for_keywords(&zettel, &searched_keywords, all) {
            matching_zettels.insert(key.to_path_buf());
        }
    }
    matching_zettels
}

/// Takes a reference to a Zettel `zettel` and a vector of Strings. Returns 
/// `true` if the zettel lists one of the strings as a keyword, or – if `all`  
/// is true – if the zettel lists all of them as keywords.
// only called by `search_index_for_keywords`.
fn search_zettel_for_keywords(zettel: &Zettel, 
              searched_keywords: &Vec<String>,
              all: bool) -> bool {
    if all {
        // All of searched_keywords need to be in zettel.keywords
        for keyword in searched_keywords {
            // so if it is NOT in, return false
            if !zettel.keywords.contains(&keyword) {
                return false;
            }
        }
        // still here? So all are in.
        return true;
    } else {
        // Just one of the searched_keywords need to be in zettel.keywords
        for keyword in searched_keywords {
            // so if it is in, return true
            if zettel.keywords.contains(&keyword) {
                return true;
            }
        }
        // No match, so:
        return false;
    }
}

// Title
/// Searches the index for zettels whose title matches `search_term` (exactly
/// if `exact` is true). Returns a list of matching zettels.
/// Called by a wrapper function of Index. See there for details.
pub fn search_index_for_title<T: AsRef<str>>(index: &Index, 
                                            search_term: T,
                                            exact: bool) -> HashSet<PathBuf> {
    let mut matching_zettels = HashSet::new();
    for (key, zettel) in &index.files {
        if search_zettel_title(&zettel, &search_term, exact) {
            matching_zettels.insert(key.to_path_buf());
        }
    }
    matching_zettels
}

/// Searches the title. Returns `true` if the referenced zettel's title  
/// contains the search term, or – if `exact` is true, if it exactly matches 
/// the search term.
fn search_zettel_title<T: AsRef<str>>(zettel: &Zettel,
                              search_term: T,
                              exact: bool) -> bool {
    let search_term = search_term.as_ref();
    let qs = search_term;
    let zt = zettel.title.as_str();
    
    if exact {
        trace!("exact. Returning {:?}", zt==qs);
        zt == qs
    } else {
        let qs = qs.to_ascii_lowercase();
        let zt = zt.to_ascii_lowercase();
        zt.contains(&qs)
    }
}


/// Combines searches for keywords and title. If `all` is true, 
/// zettels must match all search criteria.
pub fn combi_search<T: AsRef<str>>(index: &Index, 
                                   searched_keywords: Vec<String>,
                                   all: bool, 
                                   search_term: T,
                                   exact: bool) 
                                   -> HashSet<PathBuf> {
    let mut k_result = search_index_for_keywords(&index, 
                                             searched_keywords,
                                             all);
    let t_result = search_index_for_title(&index, search_term, exact);
    
    // Let's see if our result need to match *all* criteria
    if all {
        // We want only entries that are present in both results
        let mut temp = HashSet::new();
        for r in k_result {
            if t_result.contains(&r) {
                temp.insert(r);
            }
        }
        k_result = temp;
    } else {
        // We can just add t_result to k_result and return the latter
        for r in t_result {
            k_result.insert(r);
        }
    }
    k_result
}
// Content
// I won't implement a search for content. That's a classic job for grep or 
// ripgrep…

// --------------------------------------------------------------------------
// Inspect
// --------------------------------------------------------------------------
// The following functions for inspection are in module `sequences`:
// - `sequences`
// - `zettels_of_sequence`
// - `sequence_tree`
// - `sequence_tree_whole`
// 
// The following functions inspections are in module `zettel`:
// - `links_to`
//
// That leaves the one for incoming links. That one is here:

/// Takes a reference to the index as well as a list of zettels whose 
/// incoming links are to be inspected.
/// Returns a HashSet with the keys of other zettels linking to one (or all)
/// of the zettels, as by the `all` parameter.
pub fn inspect_incoming(index: &Index, 
                    inspected_zettels: &HashSet<PathBuf>, 
                    all: bool) 
                    -> HashSet<PathBuf> {
    let mut incoming = HashSet::new();
    // iterate over everything.
    for (other_key, other_zettel) in &index.files {
        if other_zettel.links_to(&inspected_zettels, all) {
            incoming.insert(other_key.to_path_buf());
        }
    }
    incoming
}

// --------------------------------------------------------------------------
#[cfg(test)]
mod tests {
    use super::*;
    extern crate tempfile;
    use self::tempfile::tempdir;
    use examples::*;
    use std::path::Path;
    
    fn setup() -> Index {
        let tmp_dir = tempdir().expect("Failed setting up temp directory.");
        let dir = tmp_dir.path();
        generate_examples_with_index(dir)
            .expect("Failed to generate examples");
        let file_path = dir.join("examples/config/index.yaml");
        let index = Index::from_file(&file_path).unwrap();
        index
    }
    
    #[test]
    fn test_search_zettel_for_keywords() {
        // Setup
        let index = setup();
        let z1 = index.get_zettel(Path::new("file1.md"));
        assert!(z1.is_some());
        let z1 = z1.unwrap();
        // Note: z1 has the following keywords: example, first, test
        
        // Tests:
        // One searched, contained, not all
        let searched_keywords = vec!["example".to_string()];
        assert!(search_zettel_for_keywords(&z1, &searched_keywords, false));
        
        // One searched, not contained, not all
        let searched_keywords = vec!["foo".to_string()];
        assert!(!search_zettel_for_keywords(&z1, &searched_keywords, false));
        
        // Two searched, one contained, not all
        let searched_keywords = vec!["example".to_string(),
                                     "foo".to_string()];
        assert!(search_zettel_for_keywords(&z1, &searched_keywords, false));
        
        // Two searched, none contained, not all
        let searched_keywords = vec!["foo".to_string(),
                                     "bar".to_string()];
        assert!(!search_zettel_for_keywords(&z1, &searched_keywords, false));
        
        // Two searched, one contained, all
        let searched_keywords = vec!["example".to_string(),
                                     "foo".to_string()];
        assert!(!search_zettel_for_keywords(&z1, &searched_keywords, true));
        
        // Two searched, two contained, all
        let searched_keywords = vec!["example".to_string(),
                                     "first".to_string()];
        assert!(search_zettel_for_keywords(&z1, &searched_keywords, true));
        
        // Three searched, two contained, not all
        let searched_keywords = vec!["example".to_string(),
                                     "first".to_string(),
                                     "foo".to_string()];
        assert!(search_zettel_for_keywords(&z1, &searched_keywords, false));
        
        // Three searched, two contained, all
        let searched_keywords = vec!["example".to_string(),
                                     "first".to_string(),
                                     "foo".to_string()];
        assert!(!search_zettel_for_keywords(&z1, &searched_keywords, true));
    }
    
    #[test]
    fn test_get_keywords() {
        let index = setup();
        
        let keywords = get_keywords(&index);
        assert_eq!(keywords.len(), 7);
        assert!(keywords.contains("example"));
        assert!(keywords.contains("test"));
        assert!(keywords.contains("first"));
        assert!(keywords.contains("second"));
        assert!(keywords.contains("third"));        
        assert!(keywords.contains("fourth"));        
        assert!(keywords.contains("pureyaml"));        
    }
    
    #[test]
    fn test_count_keywords() {
        let index = setup();
        
        let keyword_count = count_keywords(&index);
        assert_eq!(Some(&5), keyword_count.get("example"));
        assert_eq!(Some(&1), keyword_count.get("test"));
        assert_eq!(Some(&1), keyword_count.get("first"));
        assert_eq!(Some(&1), keyword_count.get("second"));
        assert_eq!(Some(&1), keyword_count.get("third"));
        assert_eq!(Some(&1), keyword_count.get("fourth"));
        assert_eq!(Some(&1), keyword_count.get("pureyaml"));
    }
    
    #[test]
    fn test_search_index_for_keywords() {
        let index = setup();
        
        // One searched, contained, not all
        let searched_keywords = vec!["first".to_string()];
        let matching_zettels = search_index_for_keywords(&index, 
                                                         searched_keywords,
                                                         false);
        assert_eq!(matching_zettels.len(), 1);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
        
        // Two searched, one contained, not all
        let searched_keywords = vec!["first".to_string(),
                                     "foo".to_string()];
        let matching_zettels = search_index_for_keywords(&index, 
                                                         searched_keywords,
                                                         false);
        assert_eq!(matching_zettels.len(), 1);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
        
        // Two searched, one contained, all
        let searched_keywords = vec!["first".to_string(),
                                     "foo".to_string()];
        let matching_zettels = search_index_for_keywords(&index, 
                                                         searched_keywords,
                                                         true);
        assert_eq!(matching_zettels.len(), 0);
   
    
        // Two searched, one contained in one zettel each, not all
        let searched_keywords = vec!["first".to_string(),
                                     "second".to_string()];
        let matching_zettels = search_index_for_keywords(&index, 
                                                         searched_keywords,
                                                         false);
        assert_eq!(matching_zettels.len(), 2);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
        assert!(matching_zettels.contains(&PathBuf::from("file2.md")));
        
        // Two searched, one contained in one zettel each, all
        let searched_keywords = vec!["first".to_string(),
                                     "second".to_string()];
        let matching_zettels = search_index_for_keywords(&index, 
                                                         searched_keywords,
                                                         true);
        assert_eq!(matching_zettels.len(), 0);
        
        // Two searched, two contained in one zettel, others have only one, all
        let searched_keywords = vec!["first".to_string(),
                                     "example".to_string()];
        let matching_zettels = search_index_for_keywords(&index, 
                                                         searched_keywords,
                                                         true);
        assert_eq!(matching_zettels.len(), 1);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
    }
    
    #[test]
    fn test_search_zettel_title() {
        // Setup
        let index = setup();
        let z1 = index.get_zettel(Path::new("file1.md"));
        assert!(z1.is_some());
        let z1 = z1.unwrap();
        
        // Test full match, not exact
        assert!(search_zettel_title(&z1, "File 1", false));
        
        // Test full match, exact
        assert!(search_zettel_title(&z1, "File 1", true));
        
        // Test partial match, not exact
        assert!(search_zettel_title(&z1, "File", false));
        
        // Test partial match, exact
        assert!(!search_zettel_title(&z1, "File", true));
        
        // Test no match, not exact
        assert!(!search_zettel_title(&z1, "foo", false));
        
        // Test no match, exact
        assert!(!search_zettel_title(&z1, "foo", true));
    }
    
    #[test]
    fn test_search_index_for_title() {
        let index = setup();
        
        // Test full match for one, not exact
        let matching_zettels = search_index_for_title(&index, "File 1", false);
        assert_eq!(matching_zettels.len(), 1);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
        
        // Test full match for one, exact
        let matching_zettels = search_index_for_title(&index, "File 1", true);
        assert_eq!(matching_zettels.len(), 1);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
        
        // Test partial match for five, not exact
        let matching_zettels = search_index_for_title(&index, "File", false);
        assert_eq!(matching_zettels.len(), 5);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
        assert!(matching_zettels.contains(&PathBuf::from("file2.md")));
        assert!(matching_zettels.contains(&PathBuf::from("file3.md")));
        assert!(matching_zettels.contains(&PathBuf::from("subdir/file4.md")));
        assert!(matching_zettels.contains(&PathBuf::from("subdir/file5.md")));
        
        // Test partial match for five, exact
        let matching_zettels = search_index_for_title(&index, "File", true);
        assert_eq!(matching_zettels.len(), 0);
        
        // Test no match, not exact
        let matching_zettels = search_index_for_title(&index, "foo", false);
        assert_eq!(matching_zettels.len(), 0);
        
        // Test no match, exact
        let matching_zettels = search_index_for_title(&index, "foo", true);
        assert_eq!(matching_zettels.len(), 0);
    }
    
    #[test]
    fn test_combi_search() {
        let index = setup();
        
        let searched_keywords = vec!["first".to_string()];
        
        let matching_zettels = combi_search(&index, 
                                            searched_keywords.clone(), 
                                            true, 
                                            "File", 
                                            true);
        assert_eq!(matching_zettels.len(), 0);
        
        let matching_zettels = combi_search(&index, 
                                            searched_keywords.clone(), 
                                            true, 
                                            "File", 
                                            false);
        assert_eq!(matching_zettels.len(), 1);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
        
        let matching_zettels = combi_search(&index, 
                                            searched_keywords, 
                                            true, 
                                            "File 1", 
                                            true);
        assert_eq!(matching_zettels.len(), 1);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
        
        let searched_keywords = vec!["example".to_string()];
        
        let matching_zettels = combi_search(&index, 
                                            searched_keywords.clone(), 
                                            true, 
                                            "File 1", 
                                            true);
        assert_eq!(matching_zettels.len(), 1);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
        
        let matching_zettels = combi_search(&index, 
                                            searched_keywords, 
                                            false, 
                                            "File 1", 
                                            true);
        assert_eq!(matching_zettels.len(), 5);
        assert!(matching_zettels.contains(&PathBuf::from("file1.md")));
        assert!(matching_zettels.contains(&PathBuf::from("file2.md")));
        assert!(matching_zettels.contains(&PathBuf::from("file3.md")));
        assert!(matching_zettels.contains(&PathBuf::from("subdir/file4.md")));
        assert!(matching_zettels.contains(&PathBuf::from("onlies/pure-yaml.yaml")));
    }

    #[test]
    fn test_inspect_incoming() {
        // Setup
        let index = setup();
        let mut inspected_zettels = HashSet::new();
        inspected_zettels.insert(PathBuf::from("file2.md"));
        inspected_zettels.insert(PathBuf::from("file3.md"));
        
        // Note:
        // file1 links to both
        // file2 links to file3
        // onlies/markdown-only links to file2
        
        // Test: What links to either of them?
        let incoming = inspect_incoming(&index, &inspected_zettels, false);
        assert_eq!(incoming.len(), 3);
        assert!(incoming.contains(&PathBuf::from("file1.md")));
        assert!(incoming.contains(&PathBuf::from("file2.md")));
        assert!(incoming.contains(&PathBuf::from("onlies/markdown-only.md")));
        
        // Test: What links to both of them?
        let incoming = inspect_incoming(&index, &inspected_zettels, true);
        assert_eq!(incoming.len(), 1);
        assert!(incoming.contains(&PathBuf::from("file1.md")));
        
        // What if we also search for links a non-existent file?
        let mut inspected_zettels = HashSet::new();
        inspected_zettels.insert(PathBuf::from("file2.md"));
        inspected_zettels.insert(PathBuf::from("file42.md"));

        
        // Test: What links to either of them?                            
        let incoming = inspect_incoming(&index, &inspected_zettels, false);
        assert_eq!(incoming.len(), 2);
        assert!(incoming.contains(&PathBuf::from("file1.md")));
        assert!(incoming.contains(&PathBuf::from("onlies/markdown-only.md")));
        
        // Test: What links to both of them?                            
        let incoming = inspect_incoming(&index, &inspected_zettels, true);
        assert_eq!(incoming.len(), 0);
    }
}
